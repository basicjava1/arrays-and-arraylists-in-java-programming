package student;

public class Student {
    private String name;
    private int[] listOfMarks;

    public Student(String name, int[] listOfMarks) {
        this.name = name;
        this.listOfMarks = listOfMarks;
    }


    int noOfMarks(){
        return listOfMarks.length;
    }

    int sumOfMarks(){
        int sum = 0;
        for(int values: listOfMarks)
            sum += values;
        return sum;
    }
}
